{{/*
More...
*/}}
{{- define "apache-shib.extra-secrets-volumes.app" -}}
{{- $APP_NAMESPACE := .Values.APP_NAMESPACE -}}
{{- if .Values.extra_secrets }}
## EXTRA SECRETS
{{- range $i, $extra_secret := .Values.extra_secrets }}
- name: {{ .name }}
  secret:
    secretName: {{ $APP_NAMESPACE }}-{{ .name }}
{{- end -}}
{{- end -}}
{{- end -}}
