{{/*
More...
*/}}
{{- define "apache-shib.extra-secrets-volume-mounts.app" -}}
{{- $APP_NAMESPACE := .Values.APP_NAMESPACE -}}
{{- if .Values.extra_secrets }}
## EXTRA SECRETS
{{- range $i, $extra_secret := .Values.extra_secrets }}
- name: {{ .name }}
  mountPath: {{ .mountPath }}
  subPath: {{ .subPath }}
{{- end -}}
{{- end -}}
{{- end -}}
