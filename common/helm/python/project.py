import time
import sys
import pprint
import yaml
import os

# Diagram of the apache-shib Kubernetes project:
#
#   Deployment
#     DeploymentSpec
#       PodTemplateSpec
#         PodSpec
#           Containers
#             Image
#             Port
#             Probes
#             Env
#             VolumeMounts
#         Volumes

from kubernetes.stream import stream
from kubernetes import client, config

from kubernetes.client.models.v1_object_meta       import V1ObjectMeta
from kubernetes.client.models.v1_label_selector    import *

from kubernetes.client.models.v1_deployment        import *
from kubernetes.client.models.v1_deployment_spec   import *
from kubernetes.client.models.v1_pod_template_spec import *
from kubernetes.client.models.v1_pod_spec          import *

from kubernetes.client.models.v1_container         import *
from kubernetes.client.models.v1_container_port    import *
from kubernetes.client.models.v1_env_var           import *
from kubernetes.client.models.v1_probe             import *
from kubernetes.client.models.v1_http_get_action   import *
from kubernetes.client.models.v1_volume_mount      import *

from kubernetes.client.models.v1_volume            import *
from kubernetes.client.models.v1_secret_volume_source import *

# Extra
from kubernetes.client.models.v1_pod       import *

#################################################################

## Make a class for our Apache Shib Pod


class Project():

    ATTRIBUTE_MAP = {
        'app_name':          'apache-shib',
        'app_namespace':     None,
        'image':             None,
        'image_tag':         'latest',
        'image_pull_policy': 'Always',
        'apache_port':       '80',
        'server_name':       None,
        'replicas':          1,
    }

    def __init__(self, values = None, debug = True):
        self.debug = debug

        # Algorithm to set attr_values.
        #  1. Set defaults from ATTRIBUTE_MAP (but use the lowercase version)
        #  2. If values is not None then any defined keys in value will override.
        #  3. If load_via is passed use that method to override.


        # 1. Load defaults from ATTRIBUTE_MAP.
        attr_values = Project.ATTRIBUTE_MAP.copy()

        # 2. Overwrite with passed in values
        if (values is not None):
            self.progress("[__init__] loading from the 'values' parameter")
            for key, value in values:
                attr_values[key] = value
        else:
            self.progress("[__init__] not loading from the 'values' parameter (was not provided)")

        # 3. Load from the environment variables.
        self.progress('[__init__] loading values from the environment')
        self.load_values_from_environment(attr_values)

        # Ensure that all the keys from ATTRIBUTE_MAP are defined.
        for key in Project.ATTRIBUTE_MAP:
            if (attr_values[key] is None):
                raise Exception(f"the required value '{key}' is not defined")

        # We are now ready to set the attributes
        for key in Project.ATTRIBUTE_MAP:
            self.progress(f"[__init__] setting attribute '{key}' to value '{attr_values[key]}'")
            setattr(self, key, attr_values[key])

        name = attr_values['app_name']

        self.progress('setting Container...')
        self.container = ApacheShibContainer(values=attr_values)
        if (self.debug):
            print(pprint.pformat(self.container))

        self.progress('setting PodSpec...')
        self.pod_spec = ApacheShibPodSpec(containers=[self.container])
        if (self.debug):
            print(pprint.pformat(self.pod_spec))

        self.progress('setting PodTemplateSpec...')
        self.pod_template_spec = ApacheShibPodTemplateSpec(spec=self.pod_spec)

        self.progress('setting DeploymentSpec...')
        self.deployment_spec = ApacheShibDeploymentSpec(pod_template_spec=self.pod_template_spec)

        self.progress('setting Deployment...')
        self.deployment = ApacheShibDeployment(name=name, spec=self.deployment_spec)

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.deployment.to_dict())

    def progress(self, msg):
        if (self.debug):
            print(f"progress: {msg}")

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def to_yaml(self):
        return yaml.dump(self.deployment.to_dict())

    @staticmethod
    def load_values_from_yaml(yaml_file):
        data = yaml.safe_load(open(yaml_file))
        return data

    def load_values_from_environment(self, attr_values):
        for key in Project.ATTRIBUTE_MAP:
            if (key.upper() in os.environ):
                env_val = os.environ[key.upper()]
                self.progress(f"setting attribute '{key}' from environment variable '{key.upper()}' to value '{env_val}'")
                attr_values[key] = env_val
            else:
                self.progress(f"not setting attribute '{key}' from environment (env variable '{key.upper()}' not defined)")

class ApacheShibDeployment(V1Deployment):
    def __init__(self, name, spec):
        metadata = V1ObjectMeta(name=name)
        super(ApacheShibDeployment, self).__init__(metadata=metadata, spec=spec)

class ApacheShibDeploymentSpec(V1DeploymentSpec):
    def __init__(self, pod_template_spec):
        selector = V1LabelSelector(match_labels = {'app': 'my-app'})
        super(ApacheShibDeploymentSpec, self).__init__(
            template=pod_template_spec,
            selector=selector,
            replicas = 3,
        )

class ApacheShibPodTemplateSpec(V1PodTemplateSpec):
    def __init__(self, spec):
        metadata = V1ObjectMeta(labels={'app': 'my-app'})
        super(ApacheShibPodTemplateSpec, self).__init__(metadata=metadata, spec=spec)

class ApacheShibPodSpec(V1PodSpec):
    def __init__(self, containers):
        vol_saml_crt = V1Volume(name='saml-crt',
                        secret=V1SecretVolumeSource(secret_name='something-saml-crt')
        )
        vol_saml_key = V1Volume(name='saml-key',
                        secret=V1SecretVolumeSource(secret_name='something-saml-key')
        )
        super(ApacheShibPodSpec, self).__init__(
            containers=containers,
            volumes=[vol_saml_crt, vol_saml_key],
        )

class ApacheShibContainer(V1Container):
    def __init__(self, values):
        name = values['app_name']
        super(ApacheShibContainer, self).__init__(name=name)

        # Docker image
        self.image             = values['image']
        self.image_pull_policy = values['image_pull_policy']

        # Port
        port = V1ContainerPort(name='http',
                               container_port=values['apache_port'],
                               protocol='TCP')
        self.ports = [port]

        # Readiness and liveness probes
        http_get        = V1HTTPGetAction(port="80", path='/Shibboleth.sso/Metadata')
        readiness_probe = V1Probe(http_get=http_get,
                                  initial_delay_seconds=10,
                                  period_seconds=10,
        )
        self.readiness_probe = readiness_probe
        self.liveness_probe  = readiness_probe

        # Environment variables
        server_name    = values['server_name']
        shib_entity_id = f"https://{server_name}"

        env_vars = [
            V1EnvVar(name='SERVER_NAME',    value=server_name),
            V1EnvVar(name='SHIB_ENTITY_ID', value=shib_entity_id),
        ]
        self.env = env_vars

        # Volume mounts
        vol_mounts = [
            V1VolumeMount(name='saml-crt',
                          mount_path='/etc/ssl/certs/saml-crt.pem',
                          sub_path='saml-crt.pem'
                          ),
            V1VolumeMount(name='saml-key',
                          mount_path='/etc/ssl/certs/saml-key.pem',
                          sub_path='saml-key.pem'
                          ),
        ]
        self.volume_mounts = vol_mounts

#################################################################

#values = {
#    'name':            'httpd',
#    'image':           'nginx',
#    'imagePullPolicy': 'always',
#    'SERVER_NAME':     'example.com',
#    'SHIB_ENTITY_ID':  'https://example.com',
#}
#
#yaml_file = './test.yaml'
#values = Project.load_values_from_yaml(yaml_file)
#
#config.load_kube_config()
#container = V1Container(name='nginx')
#
##container = ApacheShibContainer(values=values)
##pod_spec   = ApacheShibPodSpec(containers=[container])
##pod_template_spec = ApacheShibPodTemplateSpec(spec=pod_spec)
##deployment_spec = ApacheShibDeploymentSpec(pod_template_spec=pod_template_spec)
##deployment      = ApacheShibDeployment(name='zzzzzzz', spec=deployment_spec)

os.environ["APP_NAMESPACE"] = 'test-dev'
os.environ["IMAGE"]         = 'busybox'
os.environ["SERVER_NAME"]   = 'iedo.stanford.edu'

project = Project()

#print(project)
#print(project.to_yaml())

# Override something
project.deployment.spec.replicas = 4
print(project.to_yaml())

#print(container)

sys.exit()




api_instance = client.CoreV1Api()

def create_ns(namespace):
    ns = client.V1Namespace(metadata=client.V1ObjectMeta(name=namespace))
    resp = api_instance.create_namespace(body=ns)
    print(resp)

def create_pod(namespace):
    pod_name  = 'nginx'

    # Create a Pod with nginx
    pod_manifest = {
        'apiVersion': 'v1',
        'kind': 'Pod',
        'metadata': {
            'name': pod_name
        },
        'spec': {
            'containers': [{
                'image': 'busybox',
                'name': 'sleep',
                "args": [
                    "/bin/sh",
                    "-c",
                    "while true;do date;sleep 5; done"
                ]
            }]
        }
    }

    resp = api_instance.create_namespaced_pod(body=pod_manifest,
                                              namespace=namespace)

    while True:
        resp = api_instance.read_namespaced_pod(name=pod_name,
                                                namespace=namespace)
        if resp.status.phase != 'Pending':
            break
        time.sleep(1)
    print("Done.")

    exec_command = [
        '/bin/sh',
        '-c',
        'echo This message goes to stderr; echo This message goes to stdout']

    print("sleep a bit")
    time.sleep(3)
    print("done sleeping")

    resp = stream(api_instance.connect_get_namespaced_pod_exec,
                  pod_name,
                  namespace,
                  command=exec_command,
                  stderr=True, stdin=False,
                  stdout=True, tty=False)

    print("Response: " + resp)

#### #### #### #### #### #### #### #### #### #### ####

namespace = 'adamhl-testing'


pod_spec  = V1PodSpec(containers=[container])
pod_metadata = V1ObjectMeta(name='example-pod')

pod = V1Pod(metadata=pod_metadata, spec=pod_spec)

# Start pod?
resp = api_instance.create_namespaced_pod(body=pod,
                                          namespace=namespace)


print (pod.to_dict())

#create_pod(namespace)

#v1 = client.CoreV1Api()
#print("Listing pods with their IPs:")
#ret = v1.list_pod_for_all_namespaces(watch=False)
#for i in ret.items:
#    print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))
#
