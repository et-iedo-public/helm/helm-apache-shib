[[_TOC_]]

# Apache-Shibboleth Helm Project

## Introduction

This is a Helm project that creates a Deployment of the [Apache+Shibboleth
Docker container][2] optimized for GCP (Google Cloud Platform). The
project automatically registers with Google Cloud DNS and uses
Google's automated certificate management relieving you of the chore of
requesting and deploying certificates.

This project is intended to be used as a [_subchart_][5] for
other Helm charts.

The architecture follows the standard GCP Ingress model:
```
                                                    Kubernetes
                        +---------------------------------------------------------------------+
                        |                                                                     |
                        |                                                   +---------------+ |
+-------------+         | +-------------+          +-------------+          |   Deployment  | |
|             |         | |             |          |             |          |               | |
|   Client    |  +----> | |   Ingress   |  +---->  |   Service   |  +---->  |   +-------+   | |
|             |   SSL   | |             |          |             |          |   |  Pod  |   | |
+-------------+         | +-------------+          +-------------+          |   +-------+   | |
                        |                                                   +---------------+ |
                        |                                                                     |
                        +---------------------------------------------------------------------+
```

The client connects via HTTPS to the External HTTPS Load Balancer
provisioned by the Ingress. The Ingress sends requests to the Service over
port 80. Apache runs in the Pod and services the requests via HTTP. Note
that even though the traffic from the Ingress to the Service and from the
Service to the Pod is over HTTP, the traffic is encrypted; see ["Encryption
from the load balancer to the
backends"](https://cloud.google.com/load-balancing/docs/ssl-certificates/encryption-to-the-backends)
for more information.

This chart can run (if so configured) the GCP Cloud SQL Proxy as a
dependent Helm chart. See the "Configuration" section below for more
details.

## Requirements

* An instance of [externalDNS][1] running in your Kubernetes cluster so
that the IP address used by the Ingress will be associated with the server
name.

* A version of Kubernetes supporting ManagedCertificates. Currently only
GKE supports ManagedCertificates; see also [Using Google-managed SSL
certificates](https://cloud.google.com/kubernetes-engine/docs/how-to/managed-certs).

* An SSL policy resource; see the section
["FrontendConfig"](#frontendconfig-crd) below for more information.

## Configuration

### Kubernetes Configuration

See the file [`values.yaml`](values.yaml) for Kubernetes-level settings
including the Docker image settings.

### Application Configuration

The following two settings are required and should be overridden:

* `APP_NAMESPACE`: this is used to distinguish different instances of this
chart from others. For example, `myapp1-dev`, `myapp2-dev`,
`myapp1-uat`. Default: `apache-shib`.

* `SERVER_NAME`: The fully-qualified endpoint name for this application.
Default: `apache-shib.example.com`.

### GCP Cloud SQL Proxy Configuration

The Apache-Shibboleth Helm chart provides the option to run the [GCP Cloud
SQL
Proxy](https://github.com/rimusz/charts/tree/master/stable/gcloud-sqlproxy)
as an additional service. It does this by using the [rimusz GCP SQL Proxy
Helm
chart](https://github.com/rimusz/charts/tree/master/stable/gcloud-sqlproxy)
as a dependent chart. To enable this dependency set `gcloud-sqlproxy` to
`true`. If you _do_ decide to run GCP SQL Proxy Helm, be aware that you
will need to set several extra settings in `values.yaml` including
`gcloud-sqlproxy.instances`; these extra settings are described in detail
at the [GCP Cloud SQL Proxy
page](https://github.com/rimusz/charts/tree/master/stable/gcloud-sqlproxy). See
also
[kube-apache-shib-demo](https://code.stanford.edu/et-iedo-public/kube-apache-shib-demo)
for a worked-out example.


## Kubernetes Resources

### Deployment

[`templates/deployment.yaml`](templates/deployment.yaml) The Deployment
creates a replica set using the [apache-shib Docker][2] container. The
container runs Apache and Shibboleth and listens on port 80 to HTTP
traffic.

### Service

[`templates/service.yaml`](templates/service.yaml) A NodePort Service is
created that listens on port 80. This service sits between the Ingress and
the Pod and is accessible only from within the VPC.

### ManagedCertificate

We create a [MangedCertificate][6] resource with the name `APP_NAMESPACE` and
subject `SERVER_NAME`.

### Ingress

[`templates/ingress.yaml`](templates/ingress.yaml) The Ingress serves as
the TLS endpoint using the DNS-registered name `SERVER_NAME`. The DNS entry
mapping the IP address to `SERVER_NAME` is created in Google DNS using
[externalDNS][1].

This chart creates a ManagedCertificate object so that GCP will
automatically provision and update the TLS certificate that the HTTP Load
Balancer uses as the termination point.

The Ingress uses a FrontendConfig to enforce the use of TLS 1.2 or later;
see the section ["FrontendConfig"](#frontendconfig) below for more
information.

We configure the Ingress to accept only connections via HTTPS. Connections
using HTTP will come back with a 404 error. (GKE supports automatic
HTTP-to-HTTPS redirects in version 1.18.10-gke.600; see [Configuring
Ingress
features](https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features)
for more information.)

#### Ingress Health Check

The Ingress uses a health-check to the backend Service to determine if the
service is running properly. We cannot use the default health-check so we
create a custom health-check using a BackendConfig. See the
["BackendConfig"](#backendconfig) section below for more details.

### Custom Resource Definitions (CRDs)

We use two [Custom Resource Definitions (CRDs)][3]: a FrontendConfig and a
BackendConfig. These two CRDs are only supported in GKE. See also
["Configuring Ingress features"][4].

#### FrontendConfig CRD

[`templates/frontend-crd.yaml`](templates/frontend-crd.yaml) This
FrontendConfig CRD points to the Google SSL policy that enforces the
minimum version of TLS as well as allowed cipher suites. The name of this
policy is set in the [`values.yaml`](values.yaml) setting
`sslpolicy`. This Google SSL Policy is _not_ managed by this Helm chart,
rather, it must be managed externally. Default value: `minimum-tls-12`.

Here is an example of how you might create this policy using Terraform:
```
# example.tf
resource "google_compute_ssl_policy" "ssl_policy_minimum_tls_12" {
  name            = "minimum-tls-12"
  profile         = "MODERN"
  min_tls_version = "TLS_1_2"
}
```

#### BackendConfig CRD

[`templates/backend-crd.yaml`](templates/backend-crd.yaml)
The Ingress uses a health-check to the backend Service to determine if the
service is running properly. By default this health-check makes a request
to the URL path "/" on port 80. That health-check does not work for our
SAML application as the request will simply be redirected to the SAML
IdP. To get around this, we create a custom health-check using a
BackendConfig CRD. This BackendConfig tells the Ingress to make an HTTP GET to
the path `/Shibboleth.sso/Metadata` which is _not_ SAML-protected.

In the BackendConfig CRD we set session affinity to `GENERATED_COOKIE` to
ensure that a client doing an initial SAML authentication will get the same
backend Pod for the entire SAML authentication process. The TTL for this
cookie should be the same length as the allowed maximum SAML
authentication session time which, as of this writing, is 5 minutes.  See
also [Google's "Configuring Ingress features" page][7] and [Shibboleth's
SP Clustering page][8].

### Secrets

This Helm chart does *not* setup or provision any Kubernetes secrets: you
must create them independently. The secrets needed are:

* `<APP_NAMESPACE>-saml-key`: the private key portion of the SAML
Service provider public/private key pair.

* `<APP_NAMESPACE>-saml-crt`: the public key portion of the SAML
Service provider public/private key pair.

#### Extra secrets

If you have more secrets than the the SAML secrets described above,
define them using the `extra_secrets` value. The secret name must be
stored with a prefix `<APP_NAMESPACE>`. Each secret must also be
defined with a `mountPath` and a `subPath`.

Example:

```
# values.yaml
APP_NAMESPACE: myapp-1
...
extra_secrets:
  - name: secret1
    mountPath: /etc/extra/my-secret1.key
    subPath: my-secret1.key
```
In this example the secret _must_ be stored as a Kubernetes Secret with
name `myapp-1-secret1` and will be found in the Pod at
`/etc/extra/my-secret1.key`. Note that the basename of `mountPath` _must_
match `subPath`.

### ConfigMaps

At this time the Apache-Shibboleth Helm chart does not support ConfigMaps.
However, any extra file-based ConfigMaps can be stored as [extra
secrets](#extra-secrets).

## SAML

The SAML entity ID for this Service Provider will be the URL
`https://SERVER_NAME` (no trailing forward slash).


[1]: https://github.com/kubernetes-sigs/external-dns

[2]: https://code.stanford.edu/orange/docker-apache-shib

[3]: https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/

[4]: https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features

[5]: https://helm.sh/docs/chart_template_guide/subcharts_and_globals/

[6]: https://github.com/GoogleCloudPlatform/gke-managed-certs

[7]: https://cloud.google.com/kubernetes-engine/docs/how-to/ingress-features#session_affinity

[8]: https://wiki.shibboleth.net/confluence/display/SP3/Clustering
